FROM ubuntu:20.04
### FROM debian:9

LABEL maintainer="rickdavis404@gmail.com"

ENV DEBIAN_FRONTEND noninteractive

ARG VAGRANT_VERSION=2.2.10
# https://releases.hashicorp.com/vagrant/2.2.10/vagrant_2.2.10_x86_64.deb

#Install base apts for KVM, Vagrant, libvirt, etc...
RUN sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list && \
    apt update && \
    apt install -y \
      qemu-kvm \
      libvirt-daemon-system \
      libvirt-clients \
      bridge-utils \
      virt-manager \
      ebtables \
      dnsmasq-base \
      curl \
      wget \
      git \
      zip unzip \
      && \
    apt build-dep -y \
      ruby-libvirt \
      && \
    curl \
          https://releases.hashicorp.com/vagrant/${VAGRANT_VERSION}/vagrant_${VAGRANT_VERSION}_x86_64.deb \
          -o vagrant.deb \
          && \
    apt install -y ./vagrant.deb && \
    rm -f ./vagrant.deb && \
    apt autoremove -y && \
    apt clean -y

RUN curl -L \
      -o master.zip \
      https://gitlab.com/rickdavis404/vagrant-box-onie/-/jobs/826552520/artifacts/download

RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/bin/bash", "--login"]
